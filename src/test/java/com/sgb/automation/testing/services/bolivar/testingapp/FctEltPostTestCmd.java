package com.sgb.automation.testing.services.bolivar.testingapp;


import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.jayway.restassured.response.Response;
import com.sgb.automation.testing.services.bolivar.commons.commonType;
import com.sgb.automation.testing.services.bolivar.input.FctElcGeneratorDto;
import com.sgb.automation.testing.services.bolivar.model.FctElcPostModel;
import lombok.SneakyThrows;
import org.junit.Assert;

import java.sql.Timestamp;
import java.util.Date;

import static com.jayway.restassured.RestAssured.given;
import static com.sgb.automation.testing.services.bolivar.model.FctElcPostModel.*;


@SynchronousExecution
public class FctEltPostTestCmd implements BusinessLogicCommand {

    private FctElcGeneratorDto input;
    public static String outCome = "0";
    public static String reponseResult="null";
    public static int endStatusCode,statusExecution;
//    ConnectionBD CBD = new ConnectionBD();
    FctElcPostModel ModelFctElcPost = new FctElcPostModel();

    @SneakyThrows
    @Override
    public void execute() {

        Date date= new Date();

        Timestamp timestamp = new Timestamp(date.getTime());

        input = new FctElcGeneratorDto();

        String pruebaCapiSatelite = "30087";
        String filex = "{\n" +
                "  \"satelite\": \""+ satelite +"\",\n" +
                "  \"idIntFac\": "+idIntFac+",\n" +
                "  \"codSeccion\":  \""+codSeccion+"\",\n" +
                "  \"descSeccion\": \""+descSeccion+"\",\n" +
                "  \"codRamo\": "+codRamo+",\n" +
                "  \"descRamo\": \""+descRamo+"\",\n" +
                "  \"simSubproducto\": "+simSubproducto+",\n" +
                "  \"descSubproducto\": \""+descSubproducto+"\",\n" +
                "  \"numeroPoliza\": "+numeroPoliza+",\n" +
                "  \"numeroEndoso\": "+numeroEndoso+",\n" +
                "  \"notas\": \""+notas+"\",\n" +
                "  \"encabezado\": {\n" +
                "    \"tipoFacturaProv\": \""+tipoFacturaProv+"\",\n" +
                "    \"docCIABolivar\": \""+docCIABolivar+"\",\n" +
                "    \"numDocumento\": \""+numDocumento+"\",\n" +
                "    \"fechaFactura\": \""+fechaFactura+"\",\n" +
                "    \"tipoFacturaDIAN\": \""+tipoFacturaDIAN+"\",\n" +
                "    \"codMonDIAN\": \""+codMonDIAN+"\",\n" +
                "    \"totalLineas\": "+totalLineas+",\n" +
                "    \"tipoOperacion\": \""+tipoOperacion+"\"\n" +
                "  },\n" +
                "  \"emisor\": {\n" +
                "    \"docCIABolivar\": \""+docCIABolivar+"\",\n" +
                "    \"tipoDocCIABolivar\": \""+tipoDocCIABolivar+"\"\n" +
                "  },\n" +
                "  \"adquiriente\": {\n" +
                "    \"tipoPersona\": \""+tipoPersona+"\",\n" +
                "    \"nroDocumento\": \""+nroDocumento+"\",\n" +
                "    \"tipoDocumento\": \""+tipoDocumento+"\",\n" +
                "    \"regimen\": \""+regimen+"\",\n" +
                "    \"nombres\": \""+nombres+" \",\n" +
                "    \"apellidos\": \""+apellidos+" \",\n" +
                "    \"razonSocial\": \""+razonSocial+" \",\n" +
                "    \"nombreComercial\": \""+nombreComercial+" \",\n" +
                "    \"direccion\": \""+direccion+"\",\n" +
                "    \"pais\": \""+pais+"\",\n" +
                "    \"departamento\": \""+departamento+"\",\n" +
                "    \"ciudad\": \""+ciudad+"\",\n" +
                "    \"obligacionTributaria\": \""+obligacionTributaria+"\",\n" +
                "    \"contactos\": [\n" +
                "      {\n" +
                "        \"tipoContacto\": "+tipoContacto+",\n" +
                "        \"NombreCargoContacto\": \""+NombreCargoContacto+" \",\n" +
                "        \"correoContacto\": \""+correoContacto+"\"\n" +
                "      }\n" +
                "    ],\n" +
                "    \"codigoTributario\": "+codigoTributario+",\n" +
                "    \"digitoVerificationAdq\": "+digitoVerificationAdq+",\n" +
                "    \"porcentajeParticipacion\": "+porcentajeParticipacion+"\n" +
                "  },\n" +
                "  \"importesTotales\": {\n" +
                "    \"importePrima\": "+importePrima+",\n" +
                "    \"primaProv\": "+primaProv+",\n" +
                "    \"totalAPagar\": "+totalAPagar+",\n" +
                "    \"totalValorBrutoMasTributos\": "+totalValorBrutoMasTributos+"\n" +
                "  },\n" +
                "  \"tipoCambio\": {\n" +
                "    \"codigoMonetarioDIAN\": \""+codigoMonetarioDIAN+"\",\n" +
                "    \"codigoMonetarioConversion\": \""+codigoMonetarioConversion+"\",\n" +
                "    \"tc\": "+tc+",\n" +
                "    \"fechaEmision\": \""+fechaEmision+"\",\n" +
                "    \"baseMonetaria\": "+baseMonetaria+"\n" +
                "     },\n" +
                "	\"documentosReferencia\":[\n" +
                "    {\n" +
                "	\"referenciaUno\": \""+referenciaUno+"\", \n" +
                "	\"nroDocumento\": \""+nroDocumentoRef+"\",\n" +
                "	\"fechaEmisionDocumento\": \""+fechaEmisionDocumento+"\",\n" +
                "	\"cufe\": \""+cufe+"\"\n" +
                "            }\n" +
                "     ],\n" +
                "  \"mediosPago\": {\n" +
                "    \"medioPagoDIAN\": \""+medioPagoDIAN+"\",\n" +
                "    \"metodoPagoDIAN\": \""+metodoPagoDIAN+"\",\n" +
                "    \"fechaVencFactura\": \""+fechaVencFactura+"\"\n" +
                "  },\n" +
                "  \"conceptoNota\": {\n" +
                "    \"conceptoNotaDIAN\": "+conceptoNotaDIAN+",\n" +
                "    \"descripcionNota\": \""+descripcionNota+"\"\n" +
                "  },\n" +
                "  \"itemsFactura\": [\n" +
                "    {\n" +
                "      \"numeroLinea\": "+numeroLinea+",\n" +
                "      \"codigoProductoDIAN\": \""+codigoProductoDIAN+"\",\n" +
                "      \"cantidadProducto\": "+cantidadProducto+",\n" +
                "      \"unidadMedida\": \""+unidadMedida+"\",\n" +
                "      \"importePrima\": "+importePrima+",\n" +
                "      \"descripcionArticulo\": \""+descripcionArticulo+"\",\n" +
                "      \"totalAPagar\": "+totalAPagar+",\n" +
                "      \"cantidadReal\": "+cantidadReal+",\n" +
                "      \"codigoEstandarDIAN\": \""+codigoEstandarDIAN+"\",\n" +
                "      \"totalImpuestos\":\n" +
                "      {\n" +
                "        \"valorTributo\": "+valorTributo+",\n" +
                "        \"tipoImpuesto\": "+tipoImpuesto+",\n" +
                "        \"impuestos\":\n" +
                "        [\n" +
                "          {\n" +
                "            \"identificadorTributo\": \""+identificadorTributo+"\",\n" +
                "            \"valorTributo\": "+valorTributo+",\n" +
                "            \"baseImponible\": "+baseImponible+",\n" +
                "            \"tarifaTributo\": "+tarifaTributo+"\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    }\n" +
                "  ]\n" +
                "}";

            Response response = given ()
                .contentType("application/json")
                .body(filex)
                .when()
                .then()
                .log().ifError()
                .post(commonType.URL_PROYECT.getKey());


        System.out.println("El codigo de Estado FInal del Servicio  ANTES" + endStatusCode);

        endStatusCode = response.statusCode();

        statusExecution = 3;
        System.out.println("El codigo de Estado FInal del Servicio  FINAL" + endStatusCode);
        Assert.assertFalse(response.asString().contains("isError"));
        reponseResult = response.asString();
        System.out.println("Nuestro Response POST FE: " + reponseResult);
        Assert.assertNotNull(reponseResult);
        int expectedCodeStatusInInt = Integer.parseInt(expectedCodeStatusIn);
        response.then().statusCode(expectedCodeStatusInInt);
        outCome = "1";






    }


}
