package com.sgb.automation.testing.services.bolivar.model;


import com.sgb.automation.testing.services.bolivar.tools.ConnectionBD;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.sql.SQLException;


@Data
@Setter
@Getter
public class FctElcPostModel {

    public static  ConnectionBD CBD = new ConnectionBD();

    public static  String  satelite;
    public static  String idIntFac;
    public static  String codSeccion ;
    public static  String  descSeccion;
    public static  String    codRamo;
    public static  String    descRamo;
    public static  String    simSubproducto;
    public static  String   descSubproducto;
    public static  String    numeroPoliza;
    public static String    numeroEndoso;
    public static String   notas;
    //***  "encabezado": {  ***//
    public static String tipoFacturaProv;
    public static String    docCIABolivar;
    public static String   numDocumento;
    public static String  fechaFactura;
    public static String    tipoFacturaDIAN;
    public static String      codMonDIAN;
    public static String       totalLineas;
    public static String       tipoOperacion;

    //***   "emisor": { ***//
    public static String docCIABolivar1;
    public static String   tipoDocCIABolivar;

    //***  "adquiriente": { ***//
    public static String  tipoPersona;
    public static String  nroDocumento;
    public static String  tipoDocumento;
    public static String     regimen;
    public static String  razonSocial;
    public static String  nombreComercial;
    public static String  nombres;
    public static String  apellidos;
    public static String  direccion;
    public static String  pais;
    public static String     departamento;
    public static String     ciudad;
    public static String  tipoDocumentoTerc;
    public static String     digitoVerificationTerc;
    public static String  obligacionTributaria;

    //*** "contactos": [ ***//
    public static String   tipoContacto;
    public static String      NombreCargoContacto;
    public static String    correoContacto;
    public static String         codigoTributario;
    public static String      porcentajeParticipacion;
    public static String       digitoVerificationAdq;

    //*** "importesTotales": { ***//
    public static String   importePrima;
    public static String primaProv;
    public static String totalAPagar;
    public static String totalValorBrutoMasTributos;

    //*** "tipoCambio": { ***//
    public static String codigoMonetarioDIAN;
    public static String    codigoMonetarioConversion;
    public static String tc;
    public static String  fechaEmision;
    public static String baseMonetaria;

    //*** "documentosReferencia": { ***//
    public static String referenciaUno;
    public static String nroDocumentoRef;
    public static String fechaEmisionDocumento;
    public static String cufe;


    //***    "mediosPago":  ***//
    public static String  medioPagoDIAN;
    public static String        metodoPagoDIAN;
    public static String    fechaVencFactura;


    //***    "conceptoNota": { ***//
    public static String conceptoNotaDIAN;
    public static String    descripcionNota;

    //***  "itemsFactura": [ ***//

    public static String numeroLinea;
    public static String  codigoProductoDIAN;
    public static String   cantidadProducto;
    public static String   unidadMedida;
    public static String  importePrima1;
    public static String  descripcionArticulo;
    public static String  totalAPagar1;
    public static String   cantidadReal;
    public static String   codigoEstandarDIAN;

    //***  "totalImpuestos": { ***//
    public static String  valorTributo;
    public static String tipoImpuesto;

    //***   "impuestos": [ ***//
    public static String    identificadorTributo;
    public static String      valorTributo1;
    public static String    baseImponible;
    public static String    tarifaTributo;
    public static String    expectedCodeStatusIn;

    public String  FctElcPostModelCarga(String suite, String NumCase,String satelite1) throws SQLException {

//        CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"CODSECCION");
//        while (myResultSet.next()) { satelite = myResultSet.getNString("satelite");}

        satelite = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"SATELITE");
        idIntFac =  Integer.toString((Integer.parseInt(CBD.MaxIdintfac(satelite1)) +1));
          String newIdintFac = idIntFac;
        codSeccion = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"CODSECCION");
        descSeccion  = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"descSeccion");
        codRamo   = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"codRamo");
        descRamo   = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"descRamo");
        simSubproducto   = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"simSubproducto");
        descSubproducto = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"descSubproducto");
        numeroPoliza = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"numeroPoliza");
        numeroEndoso = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"numeroEndoso");
        notas = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"notas");
        //***  "encabezado": {  ***//
        tipoFacturaProv = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"tipoFacturaProv");
        docCIABolivar = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"docCIABolivar");
        numDocumento = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"numDocumento");
        fechaFactura = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"fechaFactura");
        tipoFacturaDIAN = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"tipoFacturaDIAN");
        codMonDIAN = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"codMonDIAN");
        totalLineas = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"totalLineas");
        tipoOperacion = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"tipoOperacion");

        //***   "emisor": { ***//
        docCIABolivar1 = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"docCIABolivar");
        tipoDocCIABolivar = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"tipoDocCIABolivar");

        //***  "adquiriente": { ***//
        tipoPersona = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"tipoPersona");
        nroDocumento = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"nroDocumento");
        tipoDocumento = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"tipoDocumento");
        regimen = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"regimen");
        razonSocial = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"razonSocial");
        nombreComercial = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"nombreComercial");
        nombres = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"nombres");
        apellidos = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"apellidos");
        direccion = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"direccion");
        pais = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"pais");
        departamento = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"departamento");
        ciudad = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"ciudad");
        tipoDocumentoTerc = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"tipoDocumentoTerc");
        digitoVerificationTerc = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"digitoVerificationTerc");
        obligacionTributaria = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"obligacionTributaria");

        //*** "contactos": [ ***//
        tipoContacto = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"tipoContacto");
        NombreCargoContacto = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"NombreCargoContacto");
        correoContacto = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"correoContacto");
        codigoTributario = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"codigoTributario");
        porcentajeParticipacion = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"porcentajeParticipacion");
        digitoVerificationAdq = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"digitoVerificationAdq");

        //*** "importesTotales": { ***//
        importePrima = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"importePrima");
        primaProv = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"primaProv");
        totalAPagar = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"totalAPagar");
        totalValorBrutoMasTributos = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"totalValorBrutoMasTributos");

        //*** "tipoCambio": { ***//
        codigoMonetarioDIAN = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"codigoMonetarioDIAN");
        codigoMonetarioConversion = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"codigoMonetarioConversion");
        tc = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"tc");
        fechaEmision = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"fechaEmision");
        baseMonetaria = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"baseMonetaria");

        //*** "documentosReferencia": { ***//
        referenciaUno = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"referenciaUno");
        nroDocumentoRef = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"nroDocumentoRef");
         fechaEmisionDocumento = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"fechaEmisionDocumento");
       cufe = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"cufe");

        //***    "mediosPago": { ***//
        medioPagoDIAN = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"medioPagoDIAN");
        metodoPagoDIAN = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"metodoPagoDIAN");
        fechaVencFactura = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"fechaVencFactura");

        //***    "conceptoNota": { ***//
        conceptoNotaDIAN = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"conceptoNotaDIAN");
        descripcionNota = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"descripcionNota");

        //***  "itemsFactura": [ ***//

        numeroLinea = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"numeroLinea");
        codigoProductoDIAN = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"codigoProductoDIAN");
        cantidadProducto = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"cantidadProducto");
        unidadMedida = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"unidadMedida");
        importePrima1 = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"importePrima");
        descripcionArticulo = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"descripcionArticulo");
        totalAPagar1 = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"totalAPagar");
        cantidadReal = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"cantidadReal");
        codigoEstandarDIAN = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"codigoEstandarDIAN");

        //***  "totalImpuestos": { ***//
        valorTributo = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"valorTributo");
        tipoImpuesto = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"tipoImpuesto");

        //***   "impuestos": [ ***//
        identificadorTributo = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"identificadorTributo");
        valorTributo1 = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"valorTributo");
        baseImponible = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"baseImponible");
        tarifaTributo = CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"tarifaTributo");

        expectedCodeStatusIn =  CBD.InputDataExtraction("AUT_DATA_SERVICE_POST_FE",suite,NumCase,"EXPECTED_STATUS_CODE");
        return newIdintFac;

    }



}
