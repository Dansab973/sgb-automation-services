package com.sgb.automation.testing.services.bolivar.tools;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class RecyclableMethods {

    public static Properties prop = new Properties();


    public  RecyclableMethods() {

        try (InputStream input = new FileInputStream("./config.properties")) {
            prop.load(input);

                    } catch (IOException ex) {
            System.out.println("No se encuentra el valor de la propiedad");
            ex.printStackTrace();
        }

    }

    public String getProperties(String propertie){

        return prop.getProperty(propertie);

    }

}
