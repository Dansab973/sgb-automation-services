package com.sgb.automation.testing.services.bolivar.tools;


import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class StringGenerator {
    public String next (){return RandomStringUtils.randomAlphabetic(10);
    }



}
