package com.sgb.automation.testing.services.bolivar;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("com.sgb.automation.testing.services.bolivar")
@Import({com.sgb.automation.testing.services.test.Config.class})
public class Config {
}
