package com.sgb.automation.testing.services.bolivar.commons;

public enum commonType {

    URL_PROYECT("http://stage-fe-appbalancer-1967363514.us-east-1.elb.amazonaws.com/factura");

    private String key;

    commonType(String key) {
        this.key = key;
    }

    public String getKey() {return  key;}
    public void setKey (String key){this.key = key;}
}
