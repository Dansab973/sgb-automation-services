package com.sgb.automation.testing.services.bolivar.input;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Setter
@Getter
public class FctElcGeneratorDto {


    private String satelite;
    private String idIntFac;
    private  String codSeccion;
    private String  descSeccion;
    private  String    codRamo;
    private String    descRamo;
    private String    simSubproducto;
    private String   descSubproducto;
    private String    numeroPoliza;
    private String    numeroEndoso;
    private String   notas;
    //***  "encabezado": {  ***//
    private String tipoFacturaProv;
    private String    docCIABolivar;
    private String   numDocumento;
    private String  fechaFactura;
    private String    tipoFacturaDIAN;
    private String      codMonDIAN;
    private String       totalLineas;
    private String       tipoOperacion;

    //***   "emisor": { ***//
    private String docCIABolivar1;
    private String   tipoDocCIABolivar;

    //***  "adquiriente": { ***//
    private String  tipoPersona;
    private String  nroDocumento;
    private String  tipoDocumento;
    private String     regimen;
    private String  razonSocial;
    private String  nombreComercial;
    private String  nombres;
    private String  apellidos;
    private String  direccion;
    private String  pais;
    private String     departamento;
    private String     ciudad;
    private String  tipoDocumentoTerc;
    private String     digitoVerificationTerc;
    private String  obligacionTributaria;

    //*** "contactos": [ ***//
    private String   tipoContacto;
    private String      NombreCargoContacto;
    private String    correoContacto;
    private String         codigoTributario;
    private String      porcentajeParticipacion;
    private String       digitoVerificationAdq;

    //*** "importesTotales": { ***//
    private String   importePrima;
    private String primaProv;
    private String totalAPagar;
    private String totalValorBrutoMasTributos;


    //*** "tipoCambio": { ***//
    private String codigoMonetarioDIAN;
    private String    codigoMonetarioConversion;
    private String tc;
    private String  fechaEmision;
    private String baseMonetaria;

    //*** "documentosReferencia": { ***//
    private String  fechaEmisionDocumento;
    private String     cufe;

    //***    "mediosPago": { ***//
    private String  medioPagoDIAN;
    private String        metodoPagoDIAN;
    private String        fechaVencFactura;

    //***    "conceptoNota": { ***//
    private String conceptoNotaDIAN;
    private String    descripcionNota;

    //***  "itemsFactura": [ ***//

    private String numeroLinea;
    private String  codigoProductoDIAN;
    private String   cantidadProducto;
    private String   unidadMedida;
    private String  importePrima1;
    private String  descripcionArticulo;
    private String  totalAPagar1;
    private String   cantidadReal;
    private String   codigoEstandarDIAN;

    //***  "totalImpuestos": { ***//
    private String  valorTributo;
    private String  tipoImpuesto;

    //***   "impuestos": [ ***//
    private String    identificadorTributo;
    private String      valorTributo1;
    private String    baseImponible;
    private String    tarifaTributo;


}
