package com.sgb.automation.testing.services.bolivar;

import com.sgb.automation.testing.services.bolivar.tools.StringGenerator;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class TestAutomation  extends  AbstractTest{
    @Autowired
    private StringGenerator stringGenerator;
    @Test
    public void  testAutomation(){
        String value = stringGenerator.next();
        System.out.println("El valor de prueba" + value);
    }
}
