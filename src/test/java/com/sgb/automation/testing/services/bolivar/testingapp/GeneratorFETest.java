package com.sgb.automation.testing.services.bolivar.testingapp;

import com.sgb.automation.testing.services.bolivar.AbstractTest;
import com.sgb.automation.testing.services.bolivar.model.FctElcPostModel;
import com.sgb.automation.testing.services.bolivar.tools.ConnectionBD;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.TestFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.SQLException;

import static com.sgb.automation.testing.services.bolivar.testingapp.FctEltPostTestCmd.*;

public class GeneratorFETest extends AbstractTest {

     public static ConnectionBD CBD = new ConnectionBD();
     FctElcPostModel ModelPostFE = new FctElcPostModel();
     private static int idAutExecution,idExecutionServiceFe;
     public static String newIdIntFac,suitee,casee,satelitee,tablee;


    @Autowired
    private FctEltPostTestCmd fctEltPostTestCmd;




    @BeforeClass
    public static void BeforeClass(){
        CBD.Konnection();
    }


    @Test
    public void Case_1_Suite_1_FE_Service_POST_Simon_Fac_PersonaNaturalSinIVA() throws SQLException {
        suitee = "1"; casee = "1"; satelitee = "SIMON";
        tablee = "aut_data_service_post_fe";

        idAutExecution = CBD.InsertAutExecutionInicial("2", "0","1","2");
        idExecutionServiceFe = CBD.InsertAutExecutionServiceFEInicial(suitee,casee,"1","0",idAutExecution,"'rESPONSE nULL iNICIAL'","'FE_Service_Post_funcional_Path'","'FE_Service_Post_Simon_PersonaNaturalSinIVA'");
        newIdIntFac = ModelPostFE.FctElcPostModelCarga(suitee,casee,satelitee);
        fctEltPostTestCmd.execute();



    }
    @Test
    public void Case_2_Suite_1_FE_Service_POST_Simon_NC_PersonaNaturalSinIVA() throws SQLException {
        suitee = "1"; casee = "2"; satelitee = "SIMON";
        tablee = "aut_data_service_post_fe";

        idAutExecution = CBD.InsertAutExecutionInicial("2", "0","1","2");
        idExecutionServiceFe = CBD.InsertAutExecutionServiceFEInicial(suitee,casee,"1","0",idAutExecution,"'rESPONSE nULL iNICIAL'","'FE_Service_Post_funcional_Path'","'FE_Service_Post_Simon_PersonaNaturalSinIVA'");
        newIdIntFac = ModelPostFE.FctElcPostModelCarga(suitee,casee,satelitee);
        fctEltPostTestCmd.execute();

    }
    @Test
    public void Case_3_Suite_1_FE_Fac_Service_POST_Simon_PersonaNaturalProductoConIVA() throws SQLException {
        suitee = "1"; casee = "3"; satelitee = "SIMON";
        tablee = "aut_data_service_post_fe";

        idAutExecution = CBD.InsertAutExecutionInicial("2", "0","1","2");
        idExecutionServiceFe = CBD.InsertAutExecutionServiceFEInicial(suitee,casee,"1","0",idAutExecution,"'rESPONSE nULL iNICIAL'","'FE_Service_Post_funcional_Path'","'FE_Service_Post_Simon_PersonaNaturalSinIVA'");
        newIdIntFac = ModelPostFE.FctElcPostModelCarga(suitee,casee,satelitee);
        fctEltPostTestCmd.execute();

    }

    @Test
    public void Case_4_Suite_1_FE_Fac_Service_POST_Simon_PersonaJuridicaProductoSinIVA() throws SQLException {
        suitee = "1"; casee = "4"; satelitee = "SIMON";
        tablee = "aut_data_service_post_fe";

        idAutExecution = CBD.InsertAutExecutionInicial("2", "0","1","2");
        idExecutionServiceFe = CBD.InsertAutExecutionServiceFEInicial(suitee,casee,"1","0",idAutExecution,"'rESPONSE nULL iNICIAL'","'FE_Service_Post_funcional_Path'","'FE_Service_Post_Simon_PersonaNaturalSinIVA'");
        newIdIntFac = ModelPostFE.FctElcPostModelCarga(suitee,casee,satelitee);
        fctEltPostTestCmd.execute();

    }
    @Test
    public void Case_1_Suite_2_FE_Fac_Service_POST_VPA_PersonaNaturalProductoSinIVA() throws SQLException {
        suitee = "2"; casee = "1"; satelitee = "VPA";
        tablee = "aut_data_service_post_fe";

        idAutExecution = CBD.InsertAutExecutionInicial("2", "0","1","2");
        idExecutionServiceFe = CBD.InsertAutExecutionServiceFEInicial(suitee,casee,"1","0",idAutExecution,"'rESPONSE nULL iNICIAL'","'FE_Service_Post_funcional_Path'","'FE_Service_Post_Simon_PersonaNaturalSinIVA'");
        newIdIntFac = ModelPostFE.FctElcPostModelCarga(suitee,casee,satelitee);
        fctEltPostTestCmd.execute();

    }

    @Test
    public void Case_1_Suite_3_FE_Fac_Service_POST_SECURITAS_PersonaNaturalProductoSinIVA() throws SQLException {
        suitee = "3"; casee = "1"; satelitee = "SECURITAS_PENSIONES";
        tablee = "aut_data_service_post_fe";

        idAutExecution = CBD.InsertAutExecutionInicial("2", "0","1","2");
        idExecutionServiceFe = CBD.InsertAutExecutionServiceFEInicial(suitee,casee,"1","0",idAutExecution,"'rESPONSE nULL iNICIAL'","'FE_Service_Post_funcional_Path'","'FE_Service_Post_Simon_PersonaNaturalSinIVA'");
        newIdIntFac = ModelPostFE.FctElcPostModelCarga(suitee,casee,satelitee);
        fctEltPostTestCmd.execute();

    }

    @Test
    public void Case_2_Suite_3_FE_Fac_Service_POST_SECURITAS_RentaObligatoriaPersonaNaturalProductoSinIVA() throws SQLException {
        suitee = "3"; casee = "2"; satelitee = "SECURITAS_PENSIONES";
        tablee = "aut_data_service_post_fe";

        idAutExecution = CBD.InsertAutExecutionInicial("2", "0","1","2");
        idExecutionServiceFe = CBD.InsertAutExecutionServiceFEInicial(suitee,casee,"1","0",idAutExecution,"'rESPONSE nULL iNICIAL'","'FE_Service_Post_funcional_Path'","'FE_Service_Post_Simon_PersonaNaturalSinIVA'");
        newIdIntFac = ModelPostFE.FctElcPostModelCarga(suitee,casee,satelitee);
        fctEltPostTestCmd.execute();

    }

    @Test
    public void Case_1_Suite_4_FE_Fac_Service_POST_SAI_PersonaJuridicaProductoConIVA() throws SQLException {
        suitee = "4"; casee = "1"; satelitee = "SAI";
        tablee = "aut_data_service_post_fe";
    //PASA 200 PERO ES RECHAZADA POR DATOS  INCORRECTOS
        idAutExecution = CBD.InsertAutExecutionInicial("2", "0","1","2");
        idExecutionServiceFe = CBD.InsertAutExecutionServiceFEInicial(suitee,casee,"1","0",idAutExecution,"'rESPONSE nULL iNICIAL'","'FE_Service_Post_funcional_Path'","'FE_Service_Post_Simon_PersonaNaturalSinIVA'");
        newIdIntFac = ModelPostFE.FctElcPostModelCarga(suitee,casee,satelitee);
        fctEltPostTestCmd.execute();

    }
    @Test
    public void Case_2_Suite_4_FE_Fac_Service_POST_SAI_PersonaJuridicaProductoConIVA() throws SQLException {
        suitee = "4"; casee = "2"; satelitee = "SAI";
        tablee = "aut_data_service_post_fe";

        idAutExecution = CBD.InsertAutExecutionInicial("2", "0","1","2");
        idExecutionServiceFe = CBD.InsertAutExecutionServiceFEInicial(suitee,casee,"1","0",idAutExecution,"'rESPONSE nULL iNICIAL'","'FE_Service_Post_funcional_Path'","'FE_Service_Post_Simon_PersonaNaturalSinIVA'");
        newIdIntFac = ModelPostFE.FctElcPostModelCarga(suitee,casee,satelitee);
        fctEltPostTestCmd.execute();

    }
    @Test
    public void Case_1_Suite_5_FE_Fac_Service_POST_CAPI_Juridico() throws SQLException {
        suitee = "5"; casee = "1"; satelitee = "CAPI";
        tablee = "aut_data_service_post_fe";

        idAutExecution = CBD.InsertAutExecutionInicial("2", "0","1","2");
        idExecutionServiceFe = CBD.InsertAutExecutionServiceFEInicial(suitee,casee,"1","0",idAutExecution,"'rESPONSE nULL iNICIAL'","'FE_Service_Post_funcional_Path'","'FE_Service_Post_Simon_PersonaNaturalSinIVA'");
        newIdIntFac = ModelPostFE.FctElcPostModelCarga(suitee,casee,satelitee);
        fctEltPostTestCmd.execute();

    }
    @Test
    public void Case_2_Suite_5_FE_NC_Service_POST_CAPI_Juridico() throws SQLException {
        suitee = "5"; casee = "2"; satelitee = "CAPI";
        tablee = "aut_data_service_post_fe";

        idAutExecution = CBD.InsertAutExecutionInicial("2", "0","1","2");
        idExecutionServiceFe = CBD.InsertAutExecutionServiceFEInicial(suitee,casee,"1","0",idAutExecution,"'rESPONSE nULL iNICIAL'","'FE_Service_Post_funcional_Path'","'FE_Service_Post_Simon_PersonaNaturalSinIVA'");
        newIdIntFac = ModelPostFE.FctElcPostModelCarga(suitee,casee,satelitee);
        fctEltPostTestCmd.execute();

    }
    @Test
    public void Case_3_Suite_5_FE_FAC_Service_POST_CAPI_NATURAL() throws SQLException {
        suitee = "5"; casee = "3"; satelitee = "CAPI";
        tablee = "aut_data_service_post_fe";

        idAutExecution = CBD.InsertAutExecutionInicial("2", "0","1","2");
        idExecutionServiceFe = CBD.InsertAutExecutionServiceFEInicial(suitee,casee,"1","0",idAutExecution,"'rESPONSE nULL iNICIAL'","'FE_Service_Post_funcional_Path'","'FE_Service_Post_Simon_PersonaNaturalSinIVA'");
        newIdIntFac = ModelPostFE.FctElcPostModelCarga(suitee,casee,satelitee);
        fctEltPostTestCmd.execute();

    }

    @Test
    public void Case_4_Suite_5_FE_NC_Service_POST_CAPI_NATURAL() throws SQLException {
        suitee = "5"; casee = "4"; satelitee = "CAPI";
        tablee = "aut_data_service_post_fe";

        idAutExecution = CBD.InsertAutExecutionInicial("2", "0","1","2");
        idExecutionServiceFe = CBD.InsertAutExecutionServiceFEInicial(suitee,casee,"1","0",idAutExecution,"'rESPONSE nULL iNICIAL'","'FE_Service_Post_funcional_Path'","'FE_Service_Post_Simon_PersonaNaturalSinIVA'");
        newIdIntFac = ModelPostFE.FctElcPostModelCarga(suitee,casee,satelitee);
        fctEltPostTestCmd.execute();

    }


    @After
    @TestFactory
   public  void AfterTest() throws Exception {
//        Result result = JUnitCore.runClasses(GeneratorFETest.class);
        CBD.UpdateAutExecutionFinal(idAutExecution,outCome);
        CBD.UpdateAutExecutionServiceFEFinal(idExecutionServiceFe,endStatusCode,reponseResult);
        CBD.UpdateIdFactDataInsumo(tablee,suitee,casee,newIdIntFac);

    }
    @AfterClass
    public static void AfterClass() throws Exception {
//        Result result = JUnitCore.runClasses(GeneratorFETest.class);

        CBD.DesConnectionx();
    }

}
