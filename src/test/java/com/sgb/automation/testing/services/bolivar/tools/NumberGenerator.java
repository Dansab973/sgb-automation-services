package com.sgb.automation.testing.services.bolivar.tools;


import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class NumberGenerator {
    public String nextAssString() {
        return RandomStringUtils.randomAlphabetic(7);
    }
    public Integer nextInteger(){
        return RandomUtils.nextInt(0,Integer.MAX_VALUE);
    }
    public long nextLong(){
        return RandomUtils.nextLong(0,Long.MAX_VALUE);
    }

}
