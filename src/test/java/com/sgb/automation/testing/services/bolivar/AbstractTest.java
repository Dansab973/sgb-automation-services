package com.sgb.automation.testing.services.bolivar;


import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = {
                com.sgb.automation.testing.services.bolivar.Config.class
        }
)
public abstract class AbstractTest {
}
