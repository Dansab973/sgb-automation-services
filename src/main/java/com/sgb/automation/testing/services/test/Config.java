package com.sgb.automation.testing.services.test;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.sgb.automation.testing.services.test")


public class Config {


}
